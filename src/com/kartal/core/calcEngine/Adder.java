package com.kartal.core.calcEngine;

import com.kartal.core.calcEngine.contracts.IMathProcessing;
import com.kartal.core.calcEngine.enums.MathCommand;
import com.kartal.core.calcEngine.enums.MathSymbols;

public class Adder extends CalculateBase implements IMathProcessing {
    public Adder() {
    }

    public Adder(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftVal() + getRightVal();
        setResult(value);
    }

    @Override
    public String getKeyword() {
        return MathCommand.Add.toString().toLowerCase();
    }

    @Override
    public char getSymbol() {
        return MathSymbols.Add;
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {
        setLeftVal(leftVal);
        setRightVal(rightVal);
        calculate();
        return getResult();
    }
}
