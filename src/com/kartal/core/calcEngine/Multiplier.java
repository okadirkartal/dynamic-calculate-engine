package com.kartal.core.calcEngine;

import com.kartal.core.calcEngine.contracts.IMathProcessing;
import com.kartal.core.calcEngine.enums.MathCommand;
import com.kartal.core.calcEngine.enums.MathSymbols;

public class Multiplier extends CalculateBase implements IMathProcessing {
    public Multiplier() {
    }

    public Multiplier(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftVal() * getRightVal();
        setResult(value);
    }

    @Override
    public String getKeyword() {
        return MathCommand.Multiply.toString().toLowerCase();
    }

    @Override
    public char getSymbol() {
        return MathSymbols.Multiply;
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {
        setLeftVal(leftVal);
        setRightVal(rightVal);
        calculate();
        return getResult();
    }
}
