package com.kartal.core.calcEngine.contracts;

public interface IMathProcessing {
    String SEPERATOR = " ";

    String getKeyword();

    char getSymbol();

    double doCalculation(double leftVal, double rightVal);
}
