package com.kartal.core.calcEngine;

import com.kartal.core.calcEngine.contracts.IMathProcessing;
import com.kartal.core.calcEngine.enums.MathSymbols;

public class PowerOf extends CalculateBase implements IMathProcessing {

    public PowerOf() {
    }

    public PowerOf(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = Math.pow(getLeftVal(), getRightVal());
        setResult(value);
    }


    @Override
    public String getKeyword() {
        return "power";
    }

    @Override
    public char getSymbol() {
        return MathSymbols.Pow;
    }

    @Override
    public double doCalculation(double leftVal, double rightVal) {
        return Math.pow(leftVal, rightVal);
    }
}
