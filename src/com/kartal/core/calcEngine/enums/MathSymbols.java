package com.kartal.core.calcEngine.enums;

public class MathSymbols {
    public static final char Add = '+';
    public static final char Subtract = '-';
    public static final char Multiply = '*';
    public static final char Divider = '/';
    public static final char Pow = '^';
}
