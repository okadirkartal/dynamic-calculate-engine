package com.kartal.core.calcEngine.enums;

public enum MathCommand {
    Add, Subtract, Multiply, Divide
}
