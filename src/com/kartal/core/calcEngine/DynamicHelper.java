package com.kartal.core.calcEngine;

import com.kartal.core.calcEngine.contracts.IMathProcessing;

public class DynamicHelper {
    private IMathProcessing[] handlers;
    String[] parts;

    public DynamicHelper(IMathProcessing[] handlers) {
        this.handlers = handlers;
    }

    public String process(String statement) throws InvalidStatementException {
        //IN : add 1.0+2.0
        //OUT : 1.0 + 2.0 = 3.0

        parts = statement.split(IMathProcessing.SEPERATOR);

        if (parts.length != 3)
            throw new InvalidStatementException("Incorrect number of fields", statement);

        String keyword = parts[0]; //add

        IMathProcessing theHandler = null;

        for (IMathProcessing handler : handlers) {
            if (keyword.equalsIgnoreCase(handler.getKeyword())) {
                theHandler = handler;
                break;
            }
        }

        double leftVal, rightVal ;
        try {
            leftVal = Double.parseDouble(parts[1]);
            rightVal = Double.parseDouble(parts[2]);
        } catch (NumberFormatException e) {
            throw new InvalidStatementException("Non-numeric data", statement, e);
        }


        double result = theHandler.doCalculation(leftVal, rightVal);

        StringBuilder sb = new StringBuilder(20);
        sb.append(leftVal).append(' ').append(theHandler.getSymbol());
        sb.append(' ').append(rightVal).append(" = ");
        sb.append(result);

        return sb.toString();
    }
}
