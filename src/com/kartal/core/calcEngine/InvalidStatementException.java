package com.kartal.core.calcEngine;

public class InvalidStatementException extends Exception {

    public InvalidStatementException(String reason, String statement) {
        super(reason + " : " + statement);
    }

    public InvalidStatementException(String reson, String statement, Throwable cause) {
        super(reson + " : " + statement, cause);
    }
}
