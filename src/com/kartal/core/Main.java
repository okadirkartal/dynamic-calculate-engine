package com.kartal.core;

import com.kartal.core.calcEngine.*;
import com.kartal.core.calcEngine.contracts.IMathProcessing;

public class Main {

    public static void main(String[] args) {
        String[] statements = {
                "divide 100.0 50.0",
                "power 5.0 5.0",
                "add 25.0 92.0",
                "subtract 225.0 17.0",
                "multiply 11.0 3.0"
        };


        DynamicHelper helper = new DynamicHelper(new IMathProcessing[]{
                new Divider(), new PowerOf(), new Adder(), new Subtracter(), new Multiplier()
        });

        for (String statement : statements) {
            try {
                System.out.println(helper.process(statement));
            } catch (InvalidStatementException e) {
                System.out.println(e.getMessage());

                if (e.getCause() != null) {
                    System.out.println("Original exception : " + e.getCause().getMessage());
                }
            }
        }
    }
}